'user strict';

const sql = require('../db.js');

const Transaction = function transaction() {};

Transaction.getAllTransactionByIdUser = (idUser, result) => {
  sql.query(`SELECT * FROM transaksi WHERE idUser = ${idUser}`, (err, res) => {
    if (err) {
      console.log('error: ', err);
      result(err, null);
    } else {
      result(null, res);
    }
  });
};

Transaction.getTransactionById = (idTransaction, result) => {
  sql.query(`SELECT * FROM transaksi WHERE idTransaksi = ${idTransaction}`, (err, res) => {
    if (err) {
      console.log('error: ', err);
      result(err, null);
    } else {
      result(null, res);
    }
  });
};

Transaction.getDisableSeat = (idSchedule, result) => {
  sql.query(`SELECT chair FROM transaksi WHERE idSchedule = ${idSchedule} AND (status = "SUCCESS" OR status = "PENDING")`, (err, res) => {
    if (err) {
      console.log('error: ', err);
      result(err, null);
    } else {
      result(null, res);
    }
  });
};

Transaction.changeStatusTransaction = (body, result) => {
  const { idTransaction, status } = body;
  console.log(body);
  sql.query(`UPDATE transaksi SET status = "${status}" WHERE idTransaksi = ${idTransaction}`, (err, res) => {
    if (err) {
      console.log('error: ', err);
      result(err, null);
    } else {
      result(null, res);
    }
  });
};

Transaction.addNewTransaction = (body, result) => {
  const {
    idUser,
    idMovie,
    idSchedule,
    chair,
    virtualNumber,
  } = body;

  sql.query(
    `INSERT INTO transaksi (idUser, idMovie, idSchedule, chair, virtualNumber, status, isRate) VALUES (${idUser}, ${idMovie}, ${idSchedule}, ${chair}, ${virtualNumber}, "PENDING", 0)`,
    (e, res) => {
      if (e) {
        console.log('error on: ', e);
        result(e, null);
      } else {
        result(null, res.insertId);
      }
    },
  );
};

Transaction.changeRateTransaction = (body, result) => {
  const {
    idTransaction,
    isRate,
  } = body;

  sql.query(
    `UPDATE transaksi SET isRate = ${isRate} WHERE idTransaksi = ${idTransaction}`,
    (err, res) => {
      if (err) {
        console.log('error: ', err);
        result(err, null);
      } else {
        result(null, res);
      }
    },
  );
};

module.exports = Transaction;
