const Transaction = require('../model/Transaction.js');

/**
 * GET /getAllTransaction
 * */
exports.getAllTransaction = (req, res) => {
  const { idUser } = req.params;
  Transaction.getAllTransactionByIdUser(idUser, (err, transaction) => {
    if (err) { res.send(err); }
    res.json(transaction);
  });
};

/**
 * GET /getTransaction
 */
exports.getTransaction = (req, res) => {
  const { idTransaction } = req.params;
  Transaction.getTransactionById(idTransaction, (err, transaction) => {
    if (err) { res.send(err); }
    res.json(transaction);
  });
};

/**
 * GET /getDisableSeat
 */
exports.getDisableSeat = (req, res) => {
  const { idSchedule } = req.params;
  Transaction.getDisableSeat(idSchedule, (err, transaction) => {
    if (err) { res.send(err); }
    res.json(transaction);
  });
};

/**
 * POST /changeStatusTransaction
 */
exports.postChangeStatusTransaction = (req, res) => {
  Transaction.changeStatusTransaction(req.body, (err, transaction) => {
    if (err) {
      res.send(err);
    } else {
      res.json(transaction);
    }
  });
};

/**
 * POST /createNewTransaction
 */
exports.postNewTransaction = (req, res) => {
  Transaction.addNewTransaction(req.body, (err, transaction) => {
    if (err) {
      res.send(err);
    } else {
      res.json(transaction);
    }
  });
};

/**
 * POST /changeRateTransaction
 */
exports.postChangeRateTransaction = (req, res) => {
  Transaction.changeRateTransaction(req.body, (err, transaction) => {
    if (err) { res.send(err); }
    res.json(transaction);
  });
};
