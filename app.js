const bodyParser = require('body-parser');
const express = require('express');
const logger = require('morgan');
const cron = require('node-cron');
const dateFormat = require('dateformat');
const axios = require('axios');
const querystring = require('querystring');

const app = express();
const {
  getAllTransaction,
  getTransaction,
  getDisableSeat,
  postChangeStatusTransaction,
  postNewTransaction,
  postChangeRateTransaction,
} = require('./controller/transaction');

app.set('port', process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 3005);

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.get('/getAllTransaction/:idUser', getAllTransaction);
app.get('/getTransaction/:idTransaction/', getTransaction);
app.get('/getDisabledSeat/:idSchedule', getDisableSeat);
app.post('/changeStatusTransaction', postChangeStatusTransaction);
app.post('/createNewTransaction', postNewTransaction);
app.post('/changeRateTransaction', postChangeRateTransaction);

const insertSchedule = () => {
  const date = new Date();
  const timeNow = dateFormat(date, 'yyyy-mm-dd');
  date.setDate(date.getDate() - 7);
  const timeLater = dateFormat(date, 'yyyy-mm-dd');

  axios.get(`https://api.themoviedb.org/3/discover/movie?api_key=fc04a2bcf984886d928f8aa556dbbbf8&language=en-US&region=US&release_date.gte=${timeLater}&&release_date.lte=${timeNow}&with_release_type=2|3`)
    .then((res) => {
      res.data.results.map((val) => {
        const now = val.release_date;
        const t1 = new Date(now);
        let it = 7;
        while (it > 0) {
          setTimeout(
            () => {
              t1.setDate(t1.getDate() + 1);
              const datetime = `${dateFormat(t1, 'yyyy-mm-dd')} 15:00:00`;
              const body = { movieid: val.id, datetime: datetime }; // eslint-disable-line
              axios.post('http://ec2-54-227-190-218.compute-1.amazonaws.com/engima/public/api/insertSchedule', querystring.stringify(body)).then((response) => {
                console.log(response.data);
              }).catch((response) => {
                console.log(response);
              });
            },
            100,
          );
          it -= 1;
        }
        return true;
      });
    }).catch((err) => {
      console.log(err);
    });
};

app.listen(app.get('port'), () => {
  insertSchedule();

  console.log('App is running at http://localhost:%d', app.get('port'));
  console.log('  Press CTRL-C to stop\n');
});

cron.schedule('0 0 * * *', () => {
  insertSchedule();
});

module.exports = app;
