# WS-Transaksi

## Deskripsi Web Service Transaksi
Web service transaksi adalah sebuah service yang dibangun di atas Node.js dan mengimplementasikan 
protokol REST. Web service ini bertanggung jawab atas transaksi pembelian tiket film Engima. Layanan
yang disediakan oleh Web service ini adalah:

1. Menambah transaksi baru dengan status "Pending". Input yang diberikan adalah id pengguna, id film, kursi yang dipilih, dan nomor akun virtual yang menjadi tujuan pembayaran. Layanan mengembalikan id transaksi.
2. Mengubah status suatu transaksi menjadi status "success" atau "cancelled". Input yang diberikan adalah id transaksi.
3. Mengembalikan seluruh data transaksi pembelian film seorang pengguna Engima

## Basis Data
Basis data yang digunakan oleh web service memiliki atribut:
- idTransaksi, id dari transaksi yang dilakukan
- idUser, id dari user yang melakukan transaksi
- idMovie, id dari movie yang dipesan
- char, nomor kursi yang dipesan
- virtualNumber, nomor rekening tujuan untuk pembayaran
- status, status dari transaksi saat ini
- isRate, atribut yang menandakan sudah memberi rating atau belum
- waktu, waktu terjadinya transaksi

## Pembagian Kerja
- Menambah transaksi: 13517057
- Mengubah status transaksi: 13517078
- Mengembalikan status transaksi: 13517075
- CI/CD: 13517075
- Eksplorasi dan setup mesin deployment: 13517075

## URL
- URL deployment Web Service Transaksi: http://ec2-18-212-188-140.compute-1.amazonaws.com:3005/